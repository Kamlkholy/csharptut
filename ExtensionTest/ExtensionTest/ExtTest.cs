﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Extension;
namespace ExtensionTest
{
    [TestClass]
    public class ExtTest
    {
        [TestMethod]
        public void extractedEmailShouldMatch()
        {
            string input = "my email address is mrra_me@gmail.Com";
            string expected = "mrra_me@gmail.Com";
            Assert.AreEqual(expected, input.extractEmail(),"didn't Match");
        }
        [TestMethod]
        public void extractedQueryStringShouldMatch()
        {
            string input = "http://www.google.com.mt/search?client=firefox";
            string expected = "firefox";
            Assert.AreEqual(expected, input.extractQueryStringParamValue("client"), "didn't Match");
        }
        [TestMethod]
        public void extractNumberShouldMatch()
        {
            string input = "asjkdhs132";
            int expected = 132;
            Assert.AreEqual(expected, input.extractNumber(), 0.001, "Wrong extraction");
        }
        [TestMethod]
        public void getFirstWhenLessThanZero_ShouldThrowArgumentOutOfRange()
        {
            string input = "dsahfjfh";
            int howMany = 3;
            string expected = "dsa";
            try
            {
               string actual = input.getFirst(howMany);
            }
            catch (ArgumentOutOfRangeException e)
            {
                // assert  
                StringAssert.Contains(e.Message, Extension.Extension.lessThanZeroMsg);
                return;
            }
            Assert.AreEqual(expected, input.getFirst(howMany), "no Match");
        }
        [TestMethod]
        public void getFirstWhenExceedsLength_ShouldThrowArgumentOutOfRange()
        {
            string input = "dsahfjfh";
            int howMany = 9;
            string expected = "dsa";
            try
            {
                string actual = input.getFirst(howMany);
            }
            catch (ArgumentOutOfRangeException e)
            {
                // assert  
                StringAssert.Contains(e.Message, Extension.Extension.exceedsLengthMsg);
                return;
            }
            Assert.AreEqual(expected,input.getFirst(howMany),"no Match");
        }
        [TestMethod]
        public void getLastWhenLessThanZero_ShouldThrowArgumentOutOfRange()
        {
            string input = "dsahfjfh";
            int howMany = 3;
            string expected = "jfh";
            try
            {
                string actual = input.getLast(howMany);
            }
            catch (ArgumentOutOfRangeException e)
            {
                // assert  
                StringAssert.Contains(e.Message, Extension.Extension.lessThanZeroMsg);
                return;
            }
            Assert.AreEqual(expected, input.getLast(howMany), "no Match");
        }
        [TestMethod]
        public void getLastWhenExceedsLength_ShouldThrowArgumentOutOfRange()
        {
            string input = "dsahfjfh";
            int howMany = 3;
            string expected = "jfh";
            try
            {
                string actual = input.getLast(howMany);
            }
            catch (ArgumentOutOfRangeException e)
            {
                // assert  
                StringAssert.Contains(e.Message, Extension.Extension.exceedsLengthMsg);
                return;
            }
            Assert.AreEqual(expected, input.getLast(howMany), "no Match");
        }
        [TestMethod]
        public void isEmailTest()
        {
            string input = "asdhjh@gmail.com";
            Assert.IsTrue(input.isEmail(),"not an E-mail");
        }
        [TestMethod]
        public void isNumberTest()
        {
            string input = "23452345";
            Assert.IsTrue(input.isNumber(), "not a number");
        }
        [TestMethod]
        public void isPhoneTest()
        {
            string input = "01121209799";
            Assert.IsTrue(input.isPhone(), "not a phone");
        }
        [TestMethod]
        public void reverseTest()
        {
            string input = "asdf";
            string expected = "fdsa";
            int ass = 521;
            Assert.AreEqual(expected, input.reverse(),"didn't Match");
        }
        [TestMethod]
        public void toDoubleTest()
        {
            string input = "34682";
            double expected = 34682;
            try
            {
                input.toDouble();
            }
            catch (FormatException e)
            {
                Assert.Fail(e.Message);
                return;
            }
            Assert.AreEqual(expected, input.toDouble(), "Didn't match");
        }
        [TestMethod]
        public void toIntTest()
        {
            string input = "34682";
            double expected = 34682;
            try
            {
                input.toInt();
            }
            catch (FormatException e)
            {
                Assert.Fail(e.Message);
                return;
            }
            Assert.AreEqual(expected, input.toInt(), "Didn't match");
        }
        [TestMethod]
        public void toDateTimeTest()
        {
            string input = "34682";
            double expected = 34682;
            try
            {
                input.toDate();
            }
            catch (FormatException e)
            {
                Assert.Fail(e.Message);
                return;
            }
            Assert.AreEqual(expected, input.toDate(), "Didn't match");
        }
    }
}
