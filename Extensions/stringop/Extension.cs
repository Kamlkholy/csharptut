﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace stringop
{
    public static class Extension
    {
        public static string extractEmail(this string s)
        {
            string pattern = @"[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,10})|(([0-9]{1,3}\.){3}[0-9]{1,3}))";
            s.Trim();
            MatchCollection emails = Regex.Matches(s, pattern);
            
            if (emails.Count>0)
            {
                return emails[0].Value;
            }
            else
            {
                return "no E-mails";
            }
        }
        public static string extractQueryStringParamValue(this string s, string paramName)
        {
            string pattern1 = string.Format(@"(?<=.*(&|\?){0}=)\w+(?=&)*", paramName);
            string pattern2 = @"&.*";
            string r = Regex.Replace(s, pattern1, String.Empty);
            string r1 = Regex.Replace(r, pattern2, string.Empty);
            return Regex.Match(s,pattern1).Value;
        }
        public static int extractNumber(this string s)
        {
            string pattern = @"(\d*\d)";
            MatchCollection ms= Regex.Matches(s, pattern);
            return ms[0].Value.toInt(true);
        }
        public static string getFirst(this string s, int howMany)
        {
            s.Trim();
            return s.Substring(0,howMany);
        }
        public static string getLast(this string s, int howMany)
        {
            s.Trim();
            return s.Substring(s.Length-howMany,howMany);
        }
        public static bool isEmail(this string s)
        {
            return Regex.IsMatch(s, @"[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,10})|(([0-9]{1,3}\.){3}[0-9]{1,3}))");
        }
        public static bool isNumber(this string s)
        {
            return Regex.IsMatch(s, @"^\d*");
        }
        public static bool isPhone(this string s)
        {
            return Regex.IsMatch(s, @"\(?\+?[0-9]{1,3}\)? ?-?\(?[0-9]{1,3}\)? ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})? ?(\w{1,10}\s?\d{1,6})?");
        }
        public static string reverse(this string s)
        {
            char[] str= s.ToArray<char>();
            str = str.Reverse().ToArray<char>();

            return string.Join("",str);
        }
        public static double toDouble(this string s, bool throwExceptionIfFailed = false)
        {
                return double.Parse(s);
        }
        public static int toInt(this string s, bool throwExceptionIfFailed = false)
        {
            return int.Parse(s);
        }
        public static DateTime toDate(this string s, bool throwExceptionIfFailed = false)
        {
            return DateTime.Parse(s);
        }

    }
}
